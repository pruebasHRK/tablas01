<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSistemasApratosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sistemasApratos', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->longText('notes');

            $table->integer('familiar_id')->unsigned();

            $table->foreign('familiar_id')->references('id')->on('familiares')/*->onDelete('cascade')*/;

            $table->timestamps();
        });


        /*//familiares y sistemasApratos = familiar_sistemaAprato
        Schema::create('familiar_sistemaAprato', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('familiar_id')->unsigned();
            $table->integer('sistemaAprato_id')->unsigned();

            $table->foreign('familiar_id')->references('id')->on('familiares');
            $table->foreign('sistemaAprato_id')->references('id')->on('sistemasApratos');

            $table->timestamps();
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sistemasApratos');
    }
}
