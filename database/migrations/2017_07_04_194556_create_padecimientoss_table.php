<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePadecimientossTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('padecimientoss', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->longText('notes');

            $table->integer('sistemaAprato_id')->unsigned();
            $table->foreign('sistemaAprato_id')->references('id')->on('sistemasApratos')/*->onDelete('cascade')*/;

            $table->timestamps();
        });

        /*//sistemasAparatos y padecimiento = sistemaAparato_padecimiento
        Schema::create('sistemaAparato_padecimiento', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sistemaAparato_id')->unsigned();
            $table->integer('padecimiento_id')->unsigned();

            $table->foreign('sistemaAparato_id')->references('id')->on('sistemasAparatos');
            $table->foreign('padecimiento_id')->references('id')->on('padecimientoss');

            $table->timestamps();
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('padecimientoss');
    }
}
